parrafo = 'La logística Digital es un concepto que surge de la integración entre la logística tradicional y la era digital. Con el auge del correo electrónico y las descargas digitales reemplazando productos físicos, podríamos estar hablando de un golpe devastador para la industria de la logística, pero, de hecho, ha ocurrido algo muy diferente. El sector de la logística ha introducido las innovaciones digitales.'

def word_counter(word, parrafo):
    result = 0
    for i in range(len(parrafo)-len(word)+1):
          for j in range(len(word)):
              if parrafo[i+j] != word[j]:
                 break
          else:   
                 result+=1
    return result

print("¿Que palabra desea contar en el texto:", parrafo, "?")
palabra = input()
print("En el parrafo hay: ", word_counter(palabra, parrafo), " veces la palabra " , palabra)


